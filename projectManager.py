
import dsflow
from dsflow import DsFlowUtils
from dsflow.commons.ds_config import DSConfig, HOME_DSCONFIG_YAML
from dsflow.utils.file_utils import change_permissions_recursive

from pathlib import Path



from mlflow.tracking import MlflowClient
import mlflow
from cookiecutter.main import *

import subprocess 
import pkg_resources
from pybitbucket.auth import BasicAuthenticator
from pybitbucket.bitbucket import Client
from pybitbucket.snippet import Snippet

from pybitbucket.repository import Repository, RepositoryPayload, RepositoryForkPolicy, RepositoryRole
import jenkins
import xml.etree.ElementTree as ET

import requests

import shutil

def getEurekaConf():
    HOME_DSCONFIG_YAML = '{}/.dsflow/config.yaml'.format(str(Path.home()))
    return DsFlowUtils.parse_yaml(HOME_DSCONFIG_YAML)['eureka']

#----------create project-------------#

config_bitbucket = DsFlowUtils.parse_yaml(HOME_DSCONFIG_YAML)['bitbucket']
USER = config_bitbucket['user']
PASS = config_bitbucket['pass']
MAIL_ADRESS = config_bitbucket['mail']

config_cookiecutter = DsFlowUtils.parse_yaml(HOME_DSCONFIG_YAML)['cookiecutter']
Git_URL = config_cookiecutter['git_url']
Git_Branch = config_cookiecutter['git_branch']
OUTPUT_PATH = config_cookiecutter['output_path']
TEMPLATE_ENVIRONMENT = config_cookiecutter['template_env']
TEMPLATE_OUTPUT_PATH = config_cookiecutter['template_output_path']


class createProject:
    
    def __init__(self, project_name):
        self.project_name = project_name
        

    def create(self):
        cookiecutter(Git_URL, checkout=Git_Branch, output_dir=TEMPLATE_OUTPUT_PATH, overwrite_if_exists=True,
                     extra_context={'project_name': self.project_name}, no_input=True)
        self.activateEnv(TEMPLATE_ENVIRONMENT)
        self.deleteRepoCurl()
        git_id, git_url = self.createBitbucketRepo()
        print('git_id', git_id)
        print('git_url', git_url)
        self.pushToBitbucket(git_id, git_url)
        #return True
        
        return True
    def deleteRepoCurl(self):
     
        response  = requests.delete('https://api.bitbucket.org/2.0/repositories/Indusml/'+self.project_name, auth=(USER, PASS))
    
        if response.status_code ==  204:
            return  True
        else: return False
    
    def activateEnv(self, env):
        cmd = 'source activate ' + env
        try:
            subprocess.run(cmd, shell=True)
            return True
        except:
            return False
    def cmdExecute(self, cmd):
        try:
            subprocess.run(cmd, shell=True)
            return True
        except:
            return False
            
    def cloneProject(self, output_path, git_url):
        cmd = 'cd ' + output_path + ' && git clone ' + git_url 
  
        return self.cmdExecute(cmd)
            
    def initGit(self ):
        cmd =  'git init && git add . && git commit -m "first commit" && git remote add origin git@bitbucket.org:indusml/' + self.project_name + '.git '
  
        return self.cmdExecute(cmd)    
      
    def copyTemplateInDest(self, template_output_path, output_path):
        cmd =  'mkdir '+output_path + self.project_name + '&& cp -R ' + template_output_path + self.project_name + '/* ' + output_path + self.project_name 
  
        return self.cmdExecute(cmd)
               
     
    def deleteTemp(self, template_output_path):
        cmd =  ' rm -rf ' + template_output_path + self.project_name + '/ '
  
        return self.cmdExecute(cmd) 

      
    def pushCode(self):
        cmd =   'touch README && git add . && git commit -m "firstcommit" && git push origin master --force '
        
        return self.cmdExecute(cmd)   
        
    def runTrain(self):
     
        cmd = '  python ' + self.project_name + '/train/train.py'
        return self.cmdExecute(cmd)  
         
    def goToProejctTempPath(self, template_output_path ):
        cmd = 'cd ' + template_output_path + self.project_name  
        return self.cmdExecute(cmd)

    def goToProejctPath(self, output_path ):
        cmd = 'cd ' + output_path + self.project_name 
        return self.cmdExecute(cmd)


    def createBitbucketRepo(self):
        bitbucket = Client(BasicAuthenticator(USER, PASS, MAIL_ADRESS))
        fork_policy = RepositoryForkPolicy.ALLOW_FORKS
        repo = RepositoryPayload() \
            .add_name(self.project_name) \
            .add_is_private(False) \
            .add_fork_policy(fork_policy)
        created_repo = Repository.create(payload=repo, repository_name=self.project_name, client=bitbucket)
        print('created_repo')
        print(created_repo)
        # print(created_repo)
        foundRepo: Repository = Repository.find_repository_by_full_name(USER + '/' + self.project_name, client=bitbucket)
        return (foundRepo.data.get("uuid")[1:-1], foundRepo.data.get("links").get("clone")[1].get("href"))
    
    def pushToBitbucket(self, git_id, git_url):

        print("-----------------goToProejctTempPath-------------------\n")
        self.goToProejctTempPath(TEMPLATE_OUTPUT_PATH)
        print("-----------------copyTemplateInDest-------------------\n")
        self.copyTemplateInDest(TEMPLATE_OUTPUT_PATH, OUTPUT_PATH)
        
        print("-----------------goToProejctPath-------------------\n")
        self.goToProejctPath(OUTPUT_PATH )
        print("-----------------initGit-------------------\n")
        self.initGit()

        #self.cloneProject(TEMPLATE_OUTPUT_PATH, git_url)
        #print("pushCode----------->",OUTPUT_PATH)
        print("-----------------pushCode-------------------\n")
        self.pushCode()
        print("-----------------deleteTemp-------------------\n")
        self.deleteTemp(TEMPLATE_OUTPUT_PATH)

    
    def createItem(self, git_id, git_url):
        ds_config = DSConfig(pkg_resources)
        default_params = ds_config.dsflow_config
        delivery_conf = default_params['jenkins']
        server = jenkins.Jenkins(delivery_conf['JENKINS_URL'])
        JENKINS_TEMPLATE = delivery_conf['JENKINS_TEMPLATE']
        xmldoc = ET.parse(JENKINS_TEMPLATE)
        variables = xmldoc.findall(".//sources/data/jenkins.branch.BranchSource/source/id")
        for e in variables:
            e.text=git_id
        variables = xmldoc.findall(".//sources/data/jenkins.branch.BranchSource/source/remote")
        for e in variables:
            e.text = git_url
        xmldoc.write(JENKINS_TEMPLATE)
        xmldoc = ET.parse(JENKINS_TEMPLATE)
        root = xmldoc.getroot()
        entete = "<?xml version='1.1' encoding='UTF-8'?>\n"
        xmlstr = ET.tostring(root, encoding = 'unicode',method="xml" )
        
        
        return server.create_job(self.project_name, entete+xmlstr)
        
        
        









