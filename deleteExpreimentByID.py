
from sqlalchemy import create_engine
from sqlalchemy.sql import text


def deleteExperimentById(id):
    engine = create_engine("mysql://centos:1234@localhost:3306/indusml_db")

    statement = [text("""
                DELETE p.* from params p join runs r
                on p.run_uuid = r.run_uuid 
                where  r.lifecycle_stage = 'deleted'
                """),
                text("""
                DELETE p.*  from metrics p join runs r
                on p.run_uuid = r.run_uuid 
                where  r.lifecycle_stage = 'deleted'
                """),
                text("""
                DELETE p.*  from latest_metrics p join runs r
                on p.run_uuid = r.run_uuid 
                where  r.lifecycle_stage = 'deleted'
                """),
                text("""
                DELETE p.* from tags p join runs r
                on p.run_uuid = r.run_uuid 
                where  r.lifecycle_stage = 'deleted'
                """),
                text("""
                DELETE p.* from experiment_tags p join runs r
                on p.experiment_id = r.experiment_id 
                where  r.lifecycle_stage = 'deleted'
                """),
                text("""
                DELETE r.* FROM runs r 
                where  r.lifecycle_stage = 'deleted'
                """)]

    with engine.connect() as contextmanager:
        for sta in statement:
            contextmanager.execute(sta)
        contextmanager.execute(("DELETE r.* FROM experiments r where  r.experiment_id = '"+id+"' " ))

deleteExperimentById("33")
