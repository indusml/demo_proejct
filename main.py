import projectManager as pm
from flask import *
import py_eureka_client.eureka_client as eureka_client
from flask_cors import CORS
import argparse

app = Flask(__name__)
CORS(app)

def noUnauthorized():
    data = {
            "name" : "pulsar project manager",
            "method" : "GET",
            "code" : "401",
            "error" : "Unauthorized access"
    }
    return app.response_class(
            response=json.dumps(data),
            status=401,
            mimetype='application/json'
    )

@app.route("/", methods=['GET'])
def home():
    return noUnauthorized()

@app.route("/api/project", methods=['GET', 'POSTE'])
def api_project():
    print(request.method)
    
    if request.method == "GET": 
        return noUnauthorized()
    
@app.route("/api/projectdemo", methods=['GET', 'POSTE'])   
def demo_project():
    prg = pm.createProject("demo_project")
    print(prg.create())
    return "project demo"

if __name__ == '__main__' :

    parser = argparse.ArgumentParser()
    parser.add_argument("agent")

    args = parser.parse_args()
    config = pm.getEurekaConf()
    SERVER_PORT = 5000
    DEFAULT_EUREKA_SERVER_URL = config['server_url']

    eureka_client.init(eureka_server=DEFAULT_EUREKA_SERVER_URL,
                       app_name="PULSAR-PROJECT-MANAGER-"+args.agent,
                       instance_port=SERVER_PORT, )

    app.run(debug=True, host='0.0.0.0')